let counter = 0;

function displayMenu(){

    counter += 1;

    if((counter%2) === 1){

        document.getElementById('section1BImgIDMobile').style.display = 'none';
        document.getElementById('section1BNavID').style.display = 'flex';
        document.getElementById('section1BID').style.marginTop = '0';
        // document.getElementById('section1BNavID').style.marginTop = '200px';
        
        document.getElementById('section1ID').style.backgroundColor = 'rgba(0, 0, 0, 0.5)';
        // document.getElementById('section1ID').style.height = '130vh';
        document.getElementById('toggleID').style.backgroundImage = 'url("./images/icon-close.svg")';

    } else {

        document.getElementById('section1BImgIDMobile').style.display = 'flex';
        document.getElementById('section1BNavID').style.display = 'none';
        document.getElementById('section1BID').style.marginTop = '-150px';
        document.getElementById('section1ID').style.backgroundColor = 'hsla(200, 10%, 94%, 0.322)';
        // document.getElementById('section1ID').style.height = '150vh';
        document.getElementById('toggleID').style.backgroundImage = 'url("./images/icon-hamburger.svg")';
        
    }
}